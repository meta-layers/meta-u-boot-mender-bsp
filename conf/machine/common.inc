#@TYPE: Machine(s)
#@NAME: common include file for all (compatible) machines

#@DESCRIPTION: Machine configuration for mender/multi-v7 compatible systems

##### --> please note #####
# that multi-v7 is a dummy machine
# and actually just a copy of imx6q-phytec-mira-rdk-nand
# we try to build multi-v7 images, which are supposed to run
# on all the boards
# but can still load board specific images on boards
# the prompt (multi-v7) or (${MACHINE}) should show the running image
##### <-- please note #####

# Recipes to build that do not provide packages for
# installing into the root filesystem but building
# the image depends on the recipes:
EXTRA_IMAGEDEPENDS += "u-boot"

# the default MACHINE_EXTRA_RRECOMMENDS
# would not install the kernel modules on the core-image-minimal rootfs
# we could also build a core-image-basic to get them included or:
MACHINE_ESSENTIAL_EXTRA_RDEPENDS += " kernel-modules kernel-devicetree"

# --> compiler tuning stuff
# try this for generic armv7a image
# this is armv7 soft float - slow
# but better backwards compatible to various chips
# DEFAULTTUNE = "armv7a-neon"
# this is arm armv7 hard float - faster
# https://openbenchmarking.org/result/2009043-HU-2009045HU05
# https://openbenchmarking.org/result/2009034-HU-2009038HU98
# I guess we will use form now on
# It should work on my armv7 boards - let's see
DEFAULTTUNE = "armv7athf"
require conf/machine/include/arm/arch-armv7a.inc
# <-- compiler tuning stuff

# --> rootfs stuff
IMAGE_FSTYPES += "sdimg"
# <-- rootfs stuff

# --> serial consoles
# zedboard:  ttyPS0
# sitara:    ttyO0
# i.mx6:     ttymxc1
# i.mx6 wand:ttymxc0
# cyclone-v: ttyS0

SERIAL_CONSOLES = "115200;ttyPS0 115200;ttyO0 115200;ttymxc1 115200;ttymxc0 115200;ttyS0"
SERIAL_CONSOLES_CHECK = "${SERIAL_CONSOLES}"
# <-- serial consoles

# --> kernel stuff

# kernel version

KERNEL_VERSION_PATCHLEVEL ?= "5.4"
KERNEL_SUBLEVEL ?= "47"
PREFERRED_VERSION_linux-yocto-custom = "${KERNEL_VERSION_PATCHLEVEL}.${KERNEL_SUBLEVEL}%"

# don't comment the following out to
# use instead of "default" version of linux libc headers
# this version which is an exact fit to the kernel version
# here we don't try to use custom linux-libc-headers
# LINUXLIBCVERSION ?= "${KERNEL_VERSION_PATCHLEVEL}"

# --> standard/debug/virt
ML_DEFAULT_KERNEL := "linux-yocto-custom"
# we use a mainline defconfig and apply config fragments
# choose KTYPE for different sets of config fragments
# default KTYPE:
KTYPE ?= "std"
#KTYPE ?= "debug"
# if we use the resy-virt distro it includes conf/distro/include/ktype-virt.inc
# which sets KTYPE="virt"

# If you do not specify a LINUX_KERNEL_TYPE,
# it defaults to "standard"
LINUX_KERNEL_TYPE ?= "${KTYPE}"
# <-- standard/debug/virt

# --> prt
#PRT_KERNEL_VERSION_PATCHLEVEL="3.18"
#PRT_KERNEL_SUBLEVEL="20"
#ML_DEFAULT_KERNEL := "linux-yocto-custom"
#PREFERRED_VERSION_linux-yocto-custom = "${PRT_KERNEL_VERSION_PATCHLEVEL}.${PRT_KERNEL_SUBLEVEL}%"
#KTYPE ?= "preempt-rt"
# <-- prt

# kernel provider
PREFERRED_PROVIDER_virtual/kernel ??= "${ML_DEFAULT_KERNEL}"

KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-std = "std-linux"
KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-debug = "debug-linux"
KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-virt = "virt-linux"

KERNEL_IMAGETYPE = "zImage"
# vmlinux needed for crosstap
# KERNEL_ALT_IMAGETYPE = "vmlinux"

# in order to pick up the multi-v7-ml config stuff
KMACHINE ?= "multi-v7-ml"
# multi-v7 is a dummy machine
# and actually just a copy of imx6q-phytec-mira-rdk-nand
KMACHINE_multi-v7 = "multi-v7-ml"
KMACHINE_am335x-phytec-wega = "multi-v7-ml"
KMACHINE_imx6q-phytec-mira-rdk-nand = "multi-v7-ml"
KMACHINE_beagle-bone-black = "multi-v7-ml"
# <-- kernel stuff

# --> device tree stuff

# Mender says:
# .Found more than one dtb specified in KERNEL_DEVICETREE. 
# .Only one should be specified. Choosing the last one.
# .!!! The device tree which belongs to the board has to be last !!!

# add each new device tree only once here
KERNEL_DEVICE_TREE_ALL = "am335x-boneblack.dtb \
                          am335x-bonegreen.dtb \
                          am335x-wega-rdk.dtb \
			  imx6q-phytec-mira-rdk-nand.dtb \
			 "
# add new device tree per MACHINE here - at the end:
KERNEL_DEVICETREE_am335x-phytec-wega = "${KERNEL_DEVICE_TREE_ALL} am335x-wega-rdk.dtb"
KERNEL_DEVICETREE_imx6q-phytec-mira-rdk-nand = "${KERNEL_DEVICE_TREE_ALL} imx6q-phytec-mira-rdk-nand.dtb"
KERNEL_DEVICETREE_multi-v7 = "${KERNEL_DEVICE_TREE_ALL} imx6q-phytec-mira-rdk-nand.dtb"
KERNEL_DEVICETREE_beagle-bone-black = "${KERNEL_DEVICE_TREE_ALL} am335x-boneblack.dtb"
KERNEL_DEVICETREE_beagle-bone-green = "${KERNEL_DEVICE_TREE_ALL} am335x-bonegreen.dtb"
# <-- device tree stuff

# --> u-boot-stuff
# u-boot is used here !
DAS_UBOOT_VERSION ?= "2018.11%"
PREFERRED_VERSION_u-boot ?= "${DAS_UBOOT_VERSION}"
PREFERRED_PROVIDER_u-boot-mkimage-native ?= "u-boot-tools-native"
# so we don't accidentally pick up the one from poky
PREFERRED_VERSION_u-boot-tools-native ?= "${DAS_UBOOT_VERSION}"

# UBOOT_SUFFIX:
UBOOT_SUFFIX = "img"

# SPL_BINARY name:
SPL_BINARYNAME_am335x-phytec-wega = "MLO"
SPL_BINARYNAME_imx6q-phytec-mira-rdk-nand = "SPL"
SPL_BINARYNAME_multi-v7 = "SPL"
SPL_BINARYNAME_beagle-bone-black = "MLO"
SPL_BINARYNAME_beagle-bone-green = "MLO"
SPL_BINARYNAME_beagle-bone-black-wireless = "MLO"
SPL_BINARY = "${SPL_BINARYNAME}"

# UBOOT defconfig:
UBOOT_MACHINE_am335x-phytec-wega = "pcm051_rev3_defconfig"
UBOOT_MACHINE_imx6q-phytec-mira-rdk-nand = "pcm058_defconfig"
UBOOT_MACHINE_multi-v7 = "pcm058_defconfig"
UBOOT_MACHINE_beagle-bone-black = "am335x_evm_defconfig"
UBOOT_MACHINE_beagle-bone-green = "am335x_evm_defconfig"
# <-- u-boot-stuff

# --> wks files stuff
# .wks files are created by mender magic
do_image_wic[depends] += "mtools-native:do_populate_sysroot dosfstools-native:do_populate_sysroot u-boot:do_deploy btrfs-tools-native:do_populate_sysroot "
IMAGE_BOOT_FILES = "${KERNEL_IMAGETYPE} ${KERNEL_DEVICETREE}"
# <-- wks files stuff

# default MACHINE_FEATURES
MACHINE_FEATURES = "usbgadget usbhost vfat alsa"
